<?xml version="1.0" encoding="UTF-8"?>
<!--
<a href="http://stph.crzt.fr">Stéphane Crozat</a> 
<a href="http://www.gnu.org/licenses/gpl-3.0.txt">License GPL3.0</a>
Jun 14, 2015
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"    
    version="2.0"
	xpath-default-namespace="http://www.loc.gov/mods/v3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-2.xsd"
    
		xmlns:sc="http://www.utc.fr/ics/scenari/v3/core"
		xmlns:of="scpf.org:office" xmlns:sp="http://www.utc.fr/ics/scenari/v3/primitive">
    
    <xsl:output 
        indent="yes" 
        encoding="UTF-8"
        method="xml"/>

		

	<!--****************************************************************
	Main
	*****************************************************************-->
	

	<xsl:template match="//mods">
		<xsl:variable name="titleMods" select="normalize-space(titleInfo[not(@*)]/title)"/>
		<xsl:variable name="pattern" >\s+|'</xsl:variable>	
		<xsl:variable name="nameFile" select="replace($titleMods, $pattern, '_')"/>
		<xsl:value-of select="$nameFile" />
		<xsl:result-document href="../../output/zotero-optimplus/{$nameFile}.bib"   encoding="UTF-8" method="xml" >
			<xsl:variable name="type">
				<xsl:call-template name="genreOfDocument">
					<xsl:with-param name="genre" select="normalize-space(genre[@authority='local']/text())"/>
				</xsl:call-template>
			</xsl:variable>					
			<sc:item>		
				<xsl:element name="{concat('of:', $type)}" >
					<xsl:if test="$type!='report'">
						<xsl:attribute name="xml:id"><xsl:value-of select="generate-id(.)"/></xsl:attribute>
					</xsl:if>
					<xsl:element name="{concat('of:', $type,'M')}">	
						<sp:title><xsl:value-of select="$titleMods"/></sp:title>
						<xsl:apply-templates select="name[@type='personal' and normalize-space(descendant::roleTerm)='aut']" />
						<xsl:call-template name="contentDocument">
							<xsl:with-param name="typeOfdocument" select="$type"/>
						</xsl:call-template>			 
					</xsl:element>
				</xsl:element>
			</sc:item>
		</xsl:result-document>
	</xsl:template>

<!-- appel du template convenable pour chaque document -->
	<xsl:template name="contentDocument">
		<xsl:param name="typeOfdocument"/>
		<xsl:if test="$typeOfdocument='article' or $typeOfdocument='bookSection'or $typeOfdocument='conferencePaper'">
			<xsl:apply-templates select="descendant::relatedItem[@type='host']/titleInfo[not(@*)]/title" />
		</xsl:if>
		<xsl:if test="$typeOfdocument='conferencePaper'">
			<xsl:apply-templates select="descendant::relatedItem[@type='series']/name[@type='personal']"/>			
		</xsl:if>
		<xsl:if test="$typeOfdocument='bookSection'">
			<xsl:apply-templates select="name[@type='personal' and normalize-space(descendant::roleTerm)='ctb']" />
			<xsl:apply-templates select="descendant::extent[@unit='pages']" />
		</xsl:if>
		<xsl:if test="$typeOfdocument!='article'">
			<xsl:choose>
				<xsl:when test="$typeOfdocument='document'">
					<sp:date>
						<xsl:apply-templates select="descendant::*[contains(name(),'date')]|descendant::*[contains(name(),'Date')]" />
					</sp:date>
					<xsl:call-template name="publisher">
						<xsl:with-param name="type" select="$typeOfdocument"/>
					</xsl:call-template>
					<xsl:apply-templates select="genre[not(@*)]" /> 
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="publisher">
						<xsl:with-param name="type" select="$typeOfdocument"/>
					</xsl:call-template>
					<sp:date>
						<xsl:apply-templates select="descendant::*[contains(name(),'date')]|descendant::*[contains(name(),'Date')]" />
					</sp:date>
				</xsl:otherwise>
			</xsl:choose>            
		</xsl:if>
		<xsl:if test="$typeOfdocument='bookSection' or $typeOfdocument='book' or $typeOfdocument='conferencePaper' or  $typeOfdocument='article'">
			<xsl:call-template name="bookSection "/>
		</xsl:if>	
		<xsl:if test="$typeOfdocument='thesis'">
			<xsl:apply-templates select="genre[not(@*)]" /> 
			<sp:place><xsl:apply-templates select="descendant::placeTerm" /></sp:place>
		</xsl:if>		
		<xsl:if test="$typeOfdocument='report'">
			<sp:place>
				<xsl:apply-templates select="descendant::placeTerm" />
			</sp:place>
		</xsl:if>

		<xsl:if test="$typeOfdocument!='article'">
			<sp:url>
				<xsl:apply-templates select="descendant::url" />
			</sp:url>
		</xsl:if>		
		<xsl:call-template name="extra" />
	</xsl:template>

	<xsl:template name="bookSection">
		<xsl:if test="normalize-space(genre[@authority='local'])='journalArticle' ">
			<sp:date>
				<xsl:apply-templates select="descendant::*[contains(name(),'date')]|descendant::*[contains(name(),'Date')]" />
			</sp:date>
		</xsl:if>
		<xsl:if test="normalize-space(genre[@authority='local'])='conferencePaper' ">
			<xsl:apply-templates select="descendant::extent[@unit='pages']" />
		</xsl:if>	
		<xsl:apply-templates select="descendant::detail[@type='volume' and position() = 1]/number" />
		<xsl:if test="normalize-space(genre[@authority='local'])!='journalArticle' ">
			<xsl:apply-templates select="descendant::relatedItem[@type='series']/titleInfo/title" />
		</xsl:if> 
		<xsl:if test="normalize-space(genre[@authority='local'])='journalArticle' ">
			<xsl:apply-templates select="descendant::extent[@unit='pages']" />
			<sp:url>
				<xsl:apply-templates select="descendant::url" />
			</sp:url>
		</xsl:if> 
		<xsl:if test="normalize-space(genre[@authority='local'])!='journalArticle' ">
			<sp:place>
				<xsl:apply-templates select="descendant::placeTerm" />
			</sp:place>
		</xsl:if> 
		<xsl:apply-templates select="descendant::identifier[@type='doi']" />
		<xsl:if test="normalize-space(genre[@authority='local'])!='book' ">
			<xsl:apply-templates select="descendant::edition" />	
		</xsl:if> 
		<xsl:apply-templates select="descendant::identifier[@type='isbn']" /> 
		<xsl:apply-templates select="descendant::identifier[@type='issn']" />    
	</xsl:template>




<!-- type de document -->
	<xsl:template name="genreOfDocument">
		<xsl:param name="genre"/>		
		<xsl:choose>
			<xsl:when test="$genre='journalArticle'">
				<xsl:text>article</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$genre"/>
			</xsl:otherwise>
		</xsl:choose>	
	</xsl:template>
	<!-- authors -->	
	<xsl:template match="name[@type='personal']">
		<xsl:if test="normalize-space(role/roleTerm)='ctb'">			
			<sp:bookauthor>
				<xsl:value-of select="concat(namePart[@type='family']/text(),' ',namePart[@type='given']/text())"/>		
			</sp:bookauthor>	
		</xsl:if>
		<xsl:if test="normalize-space(role/roleTerm)='aut'">	
			<sp:author>
				<xsl:value-of select="concat(namePart[@type='family']/text(),' ',namePart[@type='given']/text())"/>		
			</sp:author>
		</xsl:if>	
		<xsl:if test="normalize-space(role/roleTerm)='pbd'">
			<sp:proceedingseditor>
				<xsl:value-of select="concat(namePart[@type='family']/text(),' ',namePart[@type='given']/text())"/>		
			</sp:proceedingseditor>				
		</xsl:if>	
	</xsl:template>
	<!-- edition de la section du livre -->
	<xsl:template match="edition">
		<sp:edition>
			<xsl:value-of select="."/>
		</sp:edition>	
	</xsl:template>
<!-- volume -->

	<xsl:template match="number">
		<sp:volume>
			<xsl:value-of select="."/>
		</sp:volume>	
	</xsl:template>
	<!-- sp:booktitle -->
	<xsl:template match="relatedItem[@type='host']/titleInfo/title">
		<xsl:if test="normalize-space(ancestor::relatedItem[@type='host']/preceding-sibling::genre[@authority='local'])='journalArticle'"	>
			<sp:journal>
				<xsl:value-of select="."/>
			</sp:journal>
		</xsl:if>
		<xsl:if test="normalize-space(ancestor::relatedItem[@type='host']/preceding-sibling::genre[@authority='local'])='book'"	>
			<sp:booktitle>
				<xsl:value-of select="."/>
			</sp:booktitle>	
		</xsl:if>
		<xsl:if test="normalize-space(ancestor::relatedItem[@type='host']/preceding-sibling::genre[@authority='local'])='conferencePaper'"	>
			<sp:proceedingstitle>
				<xsl:value-of select="."/>
			</sp:proceedingstitle>
		</xsl:if>
		<xsl:if test="normalize-space(ancestor::relatedItem[@type='host']/preceding-sibling::genre[@authority='local'])='bookSection'"	>
			<sp:booktitle>
				<xsl:value-of select="."/>
			</sp:booktitle>
		</xsl:if>
	</xsl:template>
	<!-- series -->
	<xsl:template match="relatedItem[@type='series']/titleInfo/title">
		<sp:series>
			<xsl:value-of select="."/>
		</sp:series>	
	</xsl:template>
	<!-- isbn -->
	<xsl:template match="identifier[@type='isbn']">
		<sp:isbn>
			<xsl:value-of select="."/>
		</sp:isbn>	
	</xsl:template>
	<!-- issn -->
	<xsl:template match="identifier[@type='issn']">
		<sp:issn>
			<xsl:value-of select="."/>
		</sp:issn>	
	</xsl:template>
	<!-- doi -->
	<xsl:template match="identifier[@type='doi']">
		<sp:doi>
			<xsl:value-of select="."/>
		</sp:doi>	
	</xsl:template>
	<!-- pages -->
	<xsl:template match="extent[@unit='pages']">
		<xsl:if test="list">
			<sp:pages>
				<xsl:value-of select="list"/>
			</sp:pages>	
		</xsl:if>
		<xsl:if test="start">
			<sp:pages>
				<xsl:value-of select="concat(start,'_',end)"/>
			</sp:pages>	
		</xsl:if>
	</xsl:template>
	
	<!-- type -->
	<xsl:template match="genre[not(@*)]">
		<sp:type>
			<xsl:value-of select="."/>
		</sp:type>
	</xsl:template>
<!-- publisher/university -->
	<xsl:template name="publisher">
		<xsl:param name="type"/>		
			<xsl:choose>
				<xsl:when test="normalize-space($type)='thesis'">
					<sp:university>
						<xsl:apply-templates select="descendant::publisher" />
					</sp:university>					
				</xsl:when>
				<xsl:otherwise>
					<sp:publisher>
						<xsl:apply-templates select="descendant::publisher" />
					</sp:publisher>
			</xsl:otherwise>
            </xsl:choose>	
	</xsl:template>
	<xsl:template name="extra">
		<sp:extra>
			<of:txt>
				<xsl:for-each select="note">
					<sc:para>
						<xsl:value-of select="."/>
					</sc:para>
				</xsl:for-each>			
			</of:txt>
		</sp:extra>	
	</xsl:template>	
</xsl:stylesheet>